#! /usr/bin/env node
const templates = require("./templates");
const cloner = require("./Cloner");
const console = require("./console");
const path = require("path");
const fs = require("fs");
const prompt = require("./Prompt");
const fileWalker = require("./FileHandlers/FileWalker");
const inquirer = require('inquirer');

const command = process.argv[2];  //TAKE FIRST ARGUMENT 1.list 2. build

//if the user wants to list all the remplates available
if(command=="list"){ //IF 1.list
  for (var template in templates){
    console.log(template);
  }
}
//if the user wants to build any template
else if(command=="build"){ //IF 2.build
    const startProcess = function(template){
      console.log("Fetching template...")
      cloner.clone(template,(err)=>{
        if(err) return;
        else{
          var root = process.cwd();
          var profilerPath = path.join(root,template,'profiler.json');
          //READ THE PROFILER OF THE TEMPLATE
          fs.readFile(profilerPath, 'utf8', function (err,data) {
            if(err) console.error("Failed! Could not read profiler");
            else{
                //ASK THE DETAILS ABOUT THE PROJECT
                prompt.ask(JSON.parse(data),(err,data)=>{
                    if(err) console.log(data);
                    else{
                      console.log("Building "+ data['ProjectName']);
                      fs.rename('./'+template, './'+data['ProjectName'], function (err) {
                        if (err) throw err;
                        else{
                            fileWalker.walk(path.join(root,data['ProjectName']),data,(err)=>{
                            });
                        }
                      });
                    }
                  });
              }
            });
        }
      });
    }
var template = process.argv[3];
  if(typeof(template)=="undefined"){ //IF NO TEMPLATE PROVIDED
    var allTemplates = [];
    for (var eachTemplate in templates){
      allTemplates.push(eachTemplate);
    }
    //ASK FOR TEMPLATE TO BE SELECTED
      inquirer.prompt([{
        type: 'list',
        name: 'template',
        message: 'Choose a template',
        choices: allTemplates
        }]).then(function(results){
          startProcess(results['template']);
      });
  }
  else{
    startProcess(template);
  }
}
else{
  console.error("Invalid command '"+command+"'");
}
