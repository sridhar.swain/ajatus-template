"use strict";
const prompt = require("prompt");
const caser = require("./Case");

exports.ask=function(data,callback){

  var schema = [];
  var map={};
  for (var query in data["required"]){
    var toAsk =
    {
      name : query,
      required : true,
      before : function(value){map[this.name]=value; return value;},
      description : data["required"][query]
    };
    schema.push(toAsk);
  }

  for (var query in data['default']){
    var toAsk =
    {
      name : query,
      default : data['default'][query][1],
      description : data['default'][query][0],
      before : function(value){map[this.name]=value; return value;}
    };
    schema.push(toAsk);
  }


  prompt.start();
  prompt.get(schema,(err,results)=>{
    if(err) callback(true,"Building Aborted");
    else{
      for (var query in data['auto']){
        var toMapWith = data['auto'][query][0];
        var toCase = data['auto'][query][1];
        map[query] = caser.changeCase(map[toMapWith],toCase);
      }
      callback(false,map);
    }
  });
}
