exports.changeCase=function(str,toCase){
  const toTitleCase = function(data){
    return data.split(' ')
          .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase()) //uppercase if first char else lowercase
          .join(''); // join with no spaces
  }

  const toCamelCase = function(data){
    return data.toLowerCase()
          .replace( /[-_]+/g, ' ') //remove dash and underscore
          .replace( /[^\w\s]/g, '') // remove alpha numeric
          .replace( / (.)/g, function($1) { return $1.toUpperCase(); }) // make first cahracters to upper case
          .replace( / /g, '' ); // replace space with ''

  }

  const toLowerCase = function(data){
    return data.toLowerCase()
           .replace( / /g, '' ); // replace space with ''
  }

  const toUpperCase = function(data){
    return data.toUpperCase()
          .replace( / /g, '' ); // replace space with ''
  }


  switch (toCase) {
    case 'c':
      return toCamelCase(str);
    case 'u':
      return toUpperCase(str);
    case 'l':
      return toLowerCase(str);
    case 't':
      return toTitleCase(str);
    default:
      return data;

  }
}
