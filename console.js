const chalk = require("chalk");
module.exports={
  error: function(msg){
    console.log(chalk.red(msg));
  },
  log: function(msg){
    console.log(chalk.green(msg));
  }
}
