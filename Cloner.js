var Git = require('simple-git')("./");
const chalk = require("chalk");
const templates = require("./templates");
const console = require("./console");

exports.clone= function(template,callback){
  if(typeof(templates[template])!="undefined"){
    Git.clone(templates[template],template).exec(function(err,message) {
      callback(false);
    });/*.catch(function(err){
      console.error(err);
      if(err.errno==-4){
        console.error("Failed! Remove the folder named "+template+" from current working directory");
      }
      else if(err.errno==-1){
        console.error("Failed! Make Sure you are connected to internet");
      }
      callback(true);
    });*/
  }
  else{
    console.error("Invalid template name");
  }
}
