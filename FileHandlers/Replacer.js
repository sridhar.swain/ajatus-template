const fs = require("fs");
exports.replace = function(path,data,inf,callback){
  for (var key in inf){
    data= data.split('---'+key+'---').join(inf[key]);
  }
  fs.writeFile(path, data, 'utf8', (err)=>{
    if(err) console.log("Could not write");
  });
}
