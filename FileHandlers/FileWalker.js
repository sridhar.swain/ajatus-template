"use strict"
const fs = require("fs");
const path = require("path");
const reader = require('./FileReader');
const replacer = require("./Replacer");

exports.walk=function(path,data,callback){
  var readDir = function(root){
    try{
    fs.readdir(root, (err, files) => {
    files.forEach(file => {
      //var filePath = path.join(root,file);
      var filePath = root+'/'+file;
      fs.stat(filePath,(err,stat)=>{
        if(err) return;
        else{
          if (stat.isFile()){
            if(!file.startsWith(".")){
              reader.read(filePath,(err,content)=>{
                if(err) callback(true);
                else{
                    replacer.replace(filePath,content,data,(err)=>{
                      callback(err);
                    });
                }
              });
            }
          }
          else if(stat.isDirectory()){
            if(file.startsWith(".")==false && file!='lib') {
              readDir(filePath);
            }
          }
        }
      });
    });
  });
  }
  catch(e){
    console.log(e.message);
    callback(true);
  }
}
  readDir(path);
}
